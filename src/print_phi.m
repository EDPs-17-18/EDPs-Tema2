function [ x, y ] = print_phi( omegas, rho, A, E, I, L )
    fun_r = @(omega) ( (rho * A * omega^2)/(E * I) )^(1/4);
    fun_phi = @(x, r)  cos(r.*x) - cosh(r.*x) + (sin(r.*L) + sinh(r*L))/(cos(r*L) - cosh(r*L)) * (sin(r.*x) - sinh(r.*x));

    n = length(omegas);
    x = linspace(0, L, 200);
    for i=1:n
        r = fun_r(omegas(i));
        y(i,:) = fun_phi(x, r);
        plot(x, y(i,:));
        hold on
    end
end
