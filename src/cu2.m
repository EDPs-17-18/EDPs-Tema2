function [ r, omega ] = cu2( L, rho, A, E, I, initial_guess )
    fun_inic = @(y) cosh(y)*cos(y) - 1;
    fun_omega = @(y) y^2/L^2 * ( (E*I)/(rho*A) )^(1/2);

    n = length(initial_guess);
    for i=1:n
        y(i) = fzero(fun_inic, initial_guess(i));
        r(i) = y(i)/L;
        omega(i) = fun_omega(y(i));
    end
end
