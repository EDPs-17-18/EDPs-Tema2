En primer lugar, se obtienen las derivadas de segundo y cuarto orden respecto de $t$ y $x$ respectivamente para la ecuación~\eqref{eq:ap3-w0}.
\begin{gather*}
	\frac{\partial^2 w_0}{\partial t^2} = - \Phi(x) \cos(\omega t) \omega^2
	\\
	\frac{\partial^4 w_0}{\partial x^4} = \cos(\omega t) \frac{d^4 \Phi}{d x^4}
\end{gather*}
Si sustituimos estos valores en la ecuación~\eqref{eq:ap3-q} y tenemos en cuenta que $q = 0$ obtenemos la siguiente EDO de cuarto orden:
\begin{gather*}
	- \rho A \Phi(x) \cos(\omega t) \omega^2 + EI \cos(\omega t) \frac{d \Phi^4}{d x^4} = 0
	\\
	EI \cos(\omega t) \frac{d^4 \Phi}{d x^4} = \rho A \Phi(x) \cos(\omega t) \omega^2
\end{gather*}
En esta expresión podemos intentar una solución con $\Phi(x) = e^{\lambda x}$:
\begin{gather*}
	EI \cos(\omega t) \frac{d^4 e^{\lambda x}}{d x^4} = \rho A e^{\lambda x} \cos(\omega t) \omega^2
	\\
	EI \frac{d^4 e^{\lambda x}}{d x^4} = \rho A e^{\lambda x} \omega^2 \implies EI \lambda^4 = \rho A \omega^2
	\\
	\lambda^4 = \frac{\rho A \omega^2}{EI}
\end{gather*}
y definimos $r$ tal que
\begin{gather} \label{eq:cu2-r}
	r = \left( \frac{\rho A \omega^2}{EI} \right)^{\frac{1}{4}}
\end{gather}
Así obtenemos la siguiente expresión para la función $\Phi(x)$
\begin{equation*}
	\Phi(x) = c_1 e^{rx} + c_2 e^{-rx} + c_3 \sin(rx) + c_4 \cos(rx)
\end{equation*}
donde si sustituimos las exponenciales por las respectivas funciones hiperbólicas ($e^x = \cosh(rx) + \sinh(rx)$ y $e^{-x} = \cosh(rx) - \sinh(rx)$) obtenemos
\begin{gather}
%	\Phi(x) = c_1 (\cosh(rx) + \sinh(rx)) + c_2 (\cosh(rx) - \sinh(rx)) + c_3 \sin(rx) + c_4 \cos(rx)
%	\\
	\Phi(x) = c_5 \cosh(rx) + c_6 \sinh(rx) + c_3 \sin(rx) + c_4 \cos(rx) \label{eq:cu2-phi-ctes}
\end{gather}

Conociendo que la viga está empotrada en sus extremos ($x = 0$ y $x = L$) podemos deducir que su desplazamiento y su rotación en estos puntos será nulo, por lo que
\begin{gather*}
	w_0(0, t) = \Phi(0)\cos(\omega t) = 0
	\\
	w_0(L, t) = \Phi(L)\cos(\omega t) = 0
	\\
	\\
	\frac{\partial w_0}{\partial x}(0) = \frac{d \Phi}{d x}(0) \cos(\omega t) = 0
	\\
	\frac{\partial w_0}{\partial x}(L) = \frac{d \Phi}{d x}(L) \cos(\omega t) = 0
\end{gather*}
Estas condiciones han de cumplirse para todo valor de $t$ y $\omega$, lo que implica $\Phi(0)=\Phi(L)=0$ y $\Phi'(0) = \Phi'(L) = 0$.

Así pues, hallamos primero la expresión para $\Phi'(x)$
\begin{gather*}
	\Phi'(x) = c_5 r \sinh(rx) + c_6 r \cosh(rx) + c_3 r \cos(rx) - c_4 r \sin(rx)
\end{gather*}
y después las expresiones para $\Phi(0)$ y $\Phi'(0)$, a partir de los cuales calculamos los valores de~$c_5$~y~$c_6$
\begin{gather*}
	\Phi(0) = c_5 \cosh(0) + c_6 \sinh(0) + c_3 \sin(0) + c_4 \cos(0) = 0 \implies c_5 = -c_4
	\\
	\Phi'(0) = c_5 r \sinh(0) + c_6 r \cosh(0) + c_3 r \cos(0) + c_4 r \sin(0) = 0 \implies c_6 = -c_3
\end{gather*}
que sustituimos en la ecuación~\eqref{eq:cu2-phi-ctes}
\begin{gather*}
	\Phi(x) = - c_4 \cosh(rx) - c_3 \sinh(rx) + c_3 \sin(rx) + c_4 \cos(rx)
\end{gather*}

Si utilizamos las condiciones del problema en $x=L$ obtenemos los valores de $c_3$:
\begin{gather*}
	\Phi(L) = - c_4 \cosh(rL) - c_3 \sinh(rL) + c_3 \sin(rL) + c_4 \cos(rL) = 0
	\\
	c_3 (\sin(rL) - \sinh(rL)) + c4 (\cos(rL) - \cosh(rL)) = 0
	\\
	c_3 = \frac{\cosh(rL) - \cos(rL)}{\sin(rL) - \sinh(rL)} c_4
\end{gather*}
\begin{gather*}
	\Phi'(L) = - c_4 r \sinh(rL) - c_3 r \cosh(rL) + c_3 r \cos(rL) - c_4 r \sin(rL) = 0
	\\
	c_3 (\cos(rL) - \cosh(rL)) - c_4 (\sin(rL) + \sinh(rL)) = 0
	\\
	c_3 = \frac{\sin(rL) + \sinh(rL)}{\cos(rL) - \cosh(rL)} c_4
\end{gather*}

Como se puede ver, las soluciones de $c_3$ dependen de $c_4$ así que, con la información de la que disponemos tenemos infinitas soluciones. Una posible solución sería aquella en que
\begin{gather*}
	c_4 = 1 \quad,\quad c_3 = \frac{\sin(rL) + \sinh(rL)}{\cos(rL) - \cosh(rL)}
\end{gather*}
Por lo que la ecuación final para $\Phi(x)$ nos quedaría de la siguiente forma:
\begin{equation}\label{eq:cu2-phi-solved}
	\Phi(x) = \cos(rx) - \cosh(rx) + \frac{\sin(rL) + \sinh(rL)}{\cos(rL) - \cosh(rL)} (\sin(rx) - \sinh(rx))
\end{equation}

Para obtener el valor de $r$ y, por ende, el de $\omega$, debemos utilizar ambos valores obtenidos de $c_3$ (que tienen que ser iguales):
\begin{gather*}
	\frac{\sin(rL) + \sinh(rL)}{\cos(rL) - \cosh(rL)} = \frac{\cosh(rL) - \cos(rL)}{\sin(rL) - \sinh(rL)}
	\\
	(\sin(rL) + \sinh(rL)) (\sin(rL) - \sinh(rL)) = (\cos(rL) - \cosh(rL)) (\cosh(rL) - \cos(rL))
	\\
	\sin[2](rL) + \cos[2](rL) + \cosh[2](rL) - \sinh[2](rL) = 2 \cosh(rL) \cos(rL)
	\\
	1 + 1 = 2 \cosh(rL) \cos(rL)
	\\
	1 = \cosh(rL) \cos(rL)
\end{gather*}

Aquí se puede aplicar un cambio de variable, tal que $rL = y$, resolver la ecuación anterior y aplicar el resultado a la ecuación~\eqref{eq:cu2-r}.
\begin{gather*}
	y = L \left( \frac{\rho A \omega^2}{EI} \right)^{\frac{1}{4}}
	\\
	\omega = \frac{y^2}{L^2} \left( \frac{EI}{\rho A} \right)^{\frac{1}{2}}
\end{gather*}

En el código~\ref{lst:cu2} se muestra un \textit{script} de MATLAB con el cual obtenemos valores para $r$ y $\omega$.
\lstinputlisting[language=MATLAB, caption={\textit{Script} para obtener los valores de $r$ y $\omega$.}, label={lst:cu2}]{../src/cu2.m}

Suponiendo las que las propiedades de la viga verifican $\left( \frac{EI}{\rho A} \right)^{\frac{1}{2}} = 1$ y $L = 1$, podemos obtener los siguientes valores para $r$ y sus correspondientes $\omega$.
\begin{align*}
	r &\simeq \left\lbrace 0, 4.7300, 7.8532, 10.9956, 14.1372, \dots\right\rbrace \simeq n\pi + 1.588, \forall n \in \mathbb{N}
	\\
	\omega &\simeq \left\lbrace 0, 22.3733, 61.6728, 120.9034, 199.8594, \dots \right\rbrace
\end{align*}

Con estos valores y con las condiciones anteriormente descritas representamos la gráfica de la figura~\ref{fig:cu2}.
\input{graficas/cu2}