\section{Formulación del modelo de viga de Euler-Bernoulli}

Se considera una viga como la representada en la figura~\ref{fig:ap1}. Aunque las deformaciones mecánicas de una viga pueden resolverse planteando un modelo tridimensional, las reducidas dimensiones transversales de este tipo de elementos permiten, haciendo algunas simplificaciones adicionales, plantear un modelo unidimensional cuya resolución es considerablemente menos costosa. Por esta razón el uso de elementos unidimensionales para la representación del comportamiento mecánico de vigas es muy común en el cálculo de estructuras.

\begin{figure}[htbp]
	\centering
	\begin{subfigure}[b]{0.4\linewidth}
		\centering\includegraphics[width=0.85\textwidth]{figuras/apartado1_1}
%		\caption{Lectura de una matriz por filas.}
%		\label{fig:matriz-filas}
	\end{subfigure}%
	\begin{subfigure}[b]{0.4\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/apartado1_2}
%		\caption{Lectura de una matriz por columnas.}
%		\label{fig:matriz-cols}
	\end{subfigure}
	\caption{Esquema de viga (izquierda) y fibra neutra (derecha).}
	\label{fig:ap1}
\end{figure}

Entre las posibles simplificaciones que se pueden llevar a cabo para describir las deformaciones mecánicas de una viga sometida (exclusivamente) a esfuerzos de flexión, una de las más habituales es la de Euler-Bernoulli.
De acuerdo con esta aproximación, definiendo una fibra neutra para la viga (que corresponde a aquella fibra que no experimenta, en la deformación, esfuerzos de tracción ni de compresión, como se muestra en la figura~\ref{fig:ap1}) las superficies planas normales a esta fibra neutra en la configuración inicial permanecen como superficies planas y normales a la fibra neutra en la configuración deformada.

Las simplificaciones anteriores permiten obtener una representación sencilla del campo de desplazamientos. Así, se considera una viga (originalmente) recta y con sección constante sometida a una carga transversal con una dirección constante, para la que se hará coincidir el eje $OX$ con la fibra neutra y el eje $OZ$ con la dirección de la carga.

El campo de desplazamientos normales $u(x, z)$ en la viga, asumiendo un caso estático, toma la forma
\begin{equation*}
	u(x, z) = u_0(x) - z\frac{dw_0}{dx}(x)
\end{equation*}
donde la primera componente corresponde al desplazamiento de la fibra media (que se asumirá nulo en ausencia de cargas axiales) y la segunda contribución corresponde al giro de la sección donde, asumiendo pequeñas deformaciones, éste coincidirá con la derivada del campo de desplazamientos transversales de la fibra media, $w_0(x)$. Se supondrá, finalmente, que el desplazamiento transversal es constante en toda la sección.

Las deformaciones y tensiones (normales) correspondientes a este campo de desplazamientos (asumiendo ya $u_0(x)$ nulo) se obtendrán entonces de la forma
\begin{equation*}
	\epsilon_{xx}(x, z) = -z\frac{d^2w_0}{dx^2}(x) \quad \sigma_{xx}(x, z) = E\epsilon_{xx}(x, z) = -zE\frac{d^2w_0}{dx^2}(x)
\end{equation*}
que originará un momento sobre la sección $A$ de la forma
\begin{equation*}
	M(x) = - \int_{A} z\sigma_{xx}dA = E\frac{d^2w_0}{dx^2}(x) \int_{A}z^2dA
\end{equation*}
donde la última integral corresponde al momento de inercia de la sección $A$, que denotaremos mediante $I$.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\linewidth]{figuras/apartado1_3}
	\caption{Modelo de Euler-Bernoulli.}
	\label{fig:apartado1_3}
\end{figure}

El modelo de Euler-Bernoulli describe entonces los balances de momentos lineales y angulares en términos de la función de momentos sobre las secciones, $M(x)$, y los esfuerzos transversales, $V(x)$, sobre estas mismas secciones (en la figura~\ref{fig:apartado1_3} aparecen recogidas estas variables y la convención de signos para $V$). De este modo, el equilibrio de momentos lineales se escribe en el caso estático (considérese el equilibrio de una porción infinitesimal de viga)
\begin{equation*}
	\frac{dV}{dx} = q(x)
\end{equation*}
en tanto que el equilibrio de momentos angulares devuelve (de nuevo, basta considerar el equilibrio ahora de momentos angulares sobre una porción infinitesimal de viga, tomando los momentos con respecto a una de las secciones)
\begin{equation*}
	\frac{dM}{dx} = V(x)
\end{equation*}

De este modo, a partir de ambos principios de conservación y la expresión de los momentos $M$ se obtiene
\begin{equation*}
	\frac{d^2}{dx^2}\left(EI\frac{d^2w_0}{dx^2}\right) = q(x)
\end{equation*}

De cara a la imposición de condiciones de contorno sobre los extremos de la viga, debe tenerse en cuenta la siguiente información:
\begin{itemize}
	\item Los esfuerzos transversales sobre una sección, $V(x)$, se obtienen mediante
	\begin{equation*}
		V(x) = \frac{dM}{dx}(x) = E I \frac{d^3w_0}{dx^3}(x)
	\end{equation*}
	\item Los momentos sobre una sección, $M(x)$, se obtienen mediante
	\begin{equation*}
		M(x) = E I \frac{d^2w_0}{dx^2}(x)
	\end{equation*}
	\item El giro (infinitesimal) de una sección, $\theta(x)$, se obtiene mediante
	\begin{equation*}
		\theta(x) = \frac{dw_0}{dx}(x)
	\end{equation*}
\end{itemize}
de modo que todos ellos se pueden relacionar con el desplazamiento transversal, $w_0(x)$, que es la variable en la que se reformulará el problema.