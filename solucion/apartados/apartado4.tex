\section{Formulación del modelo de placa de Kirchoff}
Se considera ahora una placa plana con un espesor constante, que supondremos mucho más reducido que las otras dos dimensiones. El modelado de la flexión de esta placa puede hacerse de modo similar al de la viga considerada anteriormente.
La teoría de Kirchhoff (o de Kirchhoff–Love) asume unas hipótesis similares a las de la teoría de Euler–Bernoulli en vigas.
Así, considerando ahora la superficie media de la placa se supondrá que los segmentos rectos normales a la superficie media se mantienen, tras la deformación, rectos y normales a la superficie media, al tiempo que se supone que el espesor de la placa no varía con la deformación.

De este modo, los desplazamientos (infinitesimales) en el plano tomarán la forma
\begin{gather*}
	u(x, y, z) = u_0(x,y) - z\frac{\partial w_0}{\partial x}(x, y)
	\\
	v(x, y, z) = v_0(x,y) - z\frac{\partial w_0}{\partial y}(x, y)
\end{gather*}
donde $u_0(x,y)$ y $v_0(x,y)$ representan los desplazamientos para la superficie media, en tanto que $\frac{\partial w_0}{\partial x}$ y $\frac{\partial w_0}{\partial y}$ representan los giros (infinitesimales). Se considerará además el caso donde solamente existen cargas transversales, por lo que se asumirá que $u_0(x,y)$ y $v_0(x,y)$ son nulos.

Como en el modelo de flexión de una viga, se usarán estas expresiones del campo de desplazamientos para obtener las componentes del tensor de deformaciones y, a partir de la ley constitutiva, las correspondientes componentes del tensor de tensiones, cuya integración (a lo largo del espesor de la placa) devolverá los momentos.

Con las convenciones indicadas en la figura~\ref{fig:apartado4} se obtiene para los momentos
\begin{figure}[tbph]
	\centering
	\includegraphics[width=0.7\linewidth]{figuras/apartado4}
	\caption{Convenciones para esfuerzos y momentos sobre placa (fuente: Eindhoven University of Technology).}
	\label{fig:apartado4}
\end{figure}
\begin{gather*}
	M_{xx} = -D\left( \frac{\partial^2 w_0}{\partial x^2} + \nu \frac{\partial^2 w0}{\partial y^2} \right)
	\\
	M_{yy} = -D\left( \nu \frac{\partial^2 w_0}{\partial x^2} + \frac{\partial^2 w0}{\partial y^2} \right)
	\\
	M_{xy} = M_{yx} = -(1- \nu) D\frac{\partial^2 w_0}{\partial x \partial y}
\end{gather*}
donde $\nu$ representa el coeficiente Poisson (una propiedad elástica del material) y $D$ está dado por
\begin{equation*}
	D = \frac{h^3 E}{12(1 - \nu^2)}
\end{equation*}
siendo $h$ el espesor de la placa y $E$ el módulo de Young (un segundo parámetro elástico del material).

Como en el caso de la viga deberán formularse las ecuaciones de conservación de momentos lineales y angulares. En el caso de los momentos lineales se obtiene (para el caso estático)
\begin{equation*}
	\frac{\partial D_{x}}{\partial x} + \frac{\partial D_{y}}{\partial y} + q = 0
\end{equation*}
donde se ha usado la notación de la figura~\ref{fig:apartado4} y $q$ representa la densidad (superficial) de cargas transversales (en la dirección del eje $OZ$).

Por otro lado, las ecuaciones de conservación de momentos angulares devuelven
\begin{gather*}
	\frac{\partial M_{xx}}{\partial x} + \frac{\partial M_{xy}}{\partial y} - D_x = 0
	\\
	\frac{\partial M_{xy}}{\partial x} + \frac{\partial M_{yy}}{\partial y} - D_y = 0
\end{gather*}

Basta finalmente eliminar los esfuerzos $D_x$ y $D_y$ (del mismo modo que se hacía para el modelo de viga) para llegar a
\begin{equation*}
	\frac{\partial^2 M_{xx}}{\partial x^2} + 2\frac{\partial^2 M_{xy}}{\partial x \partial y} + \frac{\partial^2 M_{yy}}{\partial y^2} + q = 0 
\end{equation*}
y empleando entonces las expresiones de los momentos obtener
\begin{equation*}
	D \left( \frac{\partial^4 w_0}{\partial x^4} + 2\frac{\partial^4 w_0}{\partial x^2 \partial y^2} + \frac{\partial^4 M_{yy}}{\partial y^4} \right) = q
\end{equation*}

En el caso dinámico, incorporando el término de inercia a la ecuación de conservación anterior se obtiene
\begin{equation*}
	\rho h \frac{\partial^2 w_0}{\partial t^2} + D \left( \frac{\partial^4 w_0}{\partial x^4} + 2\frac{\partial^4 w_0}{\partial x^2 \partial y^2} + \frac{\partial^4 M_{yy}}{\partial y^4} \right) = q
\end{equation*}
done $\rho$ representa la densidad del material.

Finalmente, de cara a la imposición de condiciones de contorno, debe observarse que:
\begin{itemize}
	\item Pueden obtenerse los giros (infinitesimales) de las secciones a partir de las derivadas normales de la función $w_0(x,y)$.
	
	\item Pueden obtenerse los momentos sobre las secciones a partir de las derivadas (de orden dos) del campo $w_0(x,y)$ usando las expresiones de $M_{xx}$, $M_{xy}$ y $M_{yy}$ descritas anteriormente.
	
	\item Pueden obtenerse los esfuerzos cortantes sobre una sección ($D_n = D_x n_x + D_y n_y$) a partir de las derivadas (terceras) del campo $w_0(x,y)$ usando las ecuaciones de conservación de momentos lineales (que devuelven $D_x$ y $D_y$ a partir de los momentos $M_{xx}$, $M_{xy}$ y $M_{yy}$) y las expresiones de los momentos.
\end{itemize}